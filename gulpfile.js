var gulp = require('gulp'); 
var pug = require('gulp-pug');
var watch = require('gulp-watch');
var browserSync = require('browser-sync').create();
var dest = require('gulp-dest');
var data = require('gulp-data');
var fs = require('fs');

gulp.task('moveImg', function() {
  gulp.src(['specials/*', 'img/*'])
    .pipe(gulp.dest('public/img/'))
});

gulp.task('pug',function() {
   return gulp.src('./views/**/*.pug')
   .pipe(data(function(file) {
            return JSON.parse(fs.readFileSync('specials/db.json'))
   }))
  .pipe(pug({
    doctype: 'html',
    pretty: false
 }))
 .pipe(gulp.dest('./public/'));
});

gulp.task('watch', function () {
    browserSync.init({
        server: "./public"
        // or
        // proxy: 'yourserver.dev'
    });
        
    gulp.watch('./views/**/*.pug', ['pug']);
    gulp.watch("./public/**/*.html").on('change', browserSync.reload);
});


gulp.task('default', ['pug', 'moveImg']);
